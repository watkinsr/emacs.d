(require 'package)

;;;;; Startup optimizations

;;;;;; Set garbage collection threshold

;; From https://www.reddit.com/r/emacs/comments/3kqt6e/2_easy_little_known_steps_to_speed_up_emacs_start/

(setq gc-cons-threshold-original gc-cons-threshold)
(setq gc-cons-threshold (* 1024 1024 100))

;;;;;; Set file-name-handler-alist

;; Also from https://www.reddit.com/r/emacs/comments/3kqt6e/2_easy_little_known_steps_to_speed_up_emacs_start/

(setq file-name-handler-alist-original file-name-handler-alist)
(setq file-name-handler-alist nil)

;;;;;; Set deferred timer to reset them

; (run-with-idle-timer
;  5 nil
;  (lambda ()
;    (setq gc-cons-threshold gc-cons-threshold-original)
;    (setq file-name-handler-alist file-name-handler-alist-original)
;    (makunbound 'gc-cons-threshold-original)
;    (makunbound 'file-name-handler-alist-original)
;    (message "gc-cons-threshold and file-name-handler-alist restored")))

;; (setq gc-cons-threshold-original gc-cons-threshold)
;; (setq gc-cons-threshold (* 1024 1024 100))
;; (setq gc-cons-threshold 40960000)

;; Disable tool bar / scroll bar
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)

(require 'package)

(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))

;; Initialise the packages, avoiding a re-initialisation.
(unless (bound-and-true-p package--initialized)
  (setq package-enable-at-startup nil)
  (package-initialize))

;; Make sure `use-package' is available.
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Configure `use-package' prior to loading it.
(eval-and-compile
  (setq use-package-always-ensure nil)
  (setq use-package-always-defer nil)
  (setq use-package-always-demand nil)
  (setq use-package-expand-minimally nil)
  (setq use-package-enable-imenu-support t))

(eval-when-compile
  (require 'use-package))

;; See https://github.com/emacs-evil/evil-collection/issues/60 for more details.
(setq evil-want-keybinding nil)

(require 'org)
(org-babel-load-file (expand-file-name "~/.emacs.d/emacs-init.org"))
(org-babel-load-file (expand-file-name "~/.emacs.d/watkinsr-init.org"))
